#include "csvReader.h"
#include "csvWriter.h"

using namespace std;

int main(int argc, char** argv) {

    if (argc < 2){
        return (EXIT_FAILURE);
    }

    ifstream in(argv[1]);

    if (!in){
        return (EXIT_FAILURE);
    }

    vector<vector<string>*> data;

    csvReader reader;
//    csvWriter writer;

    reader.loadCSV(in, data);

    for (vector<vector<string>*>::iterator p = data.begin(); p != data.end(); ++p) {
        delete *p;
    }

    cout << "Read File Done!" << endl;

//    try {
//        writer csv("MyFile.csv");
//        csv << "A" << "VALUE" << endRow;
//
//    } catch (const exception& ex){
//        cout << "Exception was thrown: " << ex.what() << endl;
//    }

    return 0;
}