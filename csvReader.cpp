//
// Created by EB79OJ on 2019-05-15.
//
#include "csvReader.h"

using namespace std;

void csvReader::splitCSV(const std::string &s, char c, std::vector<std::string> &v) {
    int i = 0;
    int j = s.find(c);

    while (j >= 0){
        v.push_back((s.substr(i, j - i)));
        i = ++j;
        j = s.find(c, j);

        if (j > 0){
            v.push_back((s.substr(i, s.length())));
        }
    }
}

void csvReader::loadCSV(std::istream &in, std::vector<std::vector<std::string>*> &data) {

    vector<string>* p = NULL;
    string temporal;
    while (!in.eof()){
        getline(in, temporal, '\n'); // gets next line

        p = new vector<string>();
        splitCSV(temporal, ',', *p);

        data.push_back(p);

        cout << temporal << '\n';
        temporal.clear();
    }
}