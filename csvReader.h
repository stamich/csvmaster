//
// Created by EB79OJ on 2019-05-15.
//

#ifndef CSVMASTER_CSVREADER_H
#define CSVMASTER_CSVREADER_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

class csvReader {

public:
    static void splitCSV(const std::string& s, char c, std::vector<std::string>& v);
    static void loadCSV(std::istream& in, std::vector<std::vector<std::string>*>& data);
};

#endif //CSVMASTER_CSVREADER_H
