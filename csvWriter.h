//
// Created by EB79OJ on 2019-05-15.
//

#ifndef CSVMASTER_CSVWRITER_H
#define CSVMASTER_CSVWRITER_H

#pragma once

#include <iostream>
#include <fstream>

class csvfile;

inline static csvfile& endRow(csvfile& file);
inline static csvfile& flush(csvfile& file);

class csvWriter {

    std::ofstream fs;
    const std::string separator;
public:
    csvWriter(const std::string filename, const std::string separator = ";") : fs(), separator(separator){
        fs.exceptions(std::ios::failbit | std::ios::badbit);
        fs.open(filename);
    }

    void flush(){
        fs.flush();
    }

    void endRow(){
        fs << std::endl;
    }

    csvWriter& operator << (csvWriter& (* val)(csvWriter&)){
        return val(*this);
    }

    csvWriter& operator << (const char * val){
        fs << '"' << val << '"' << separator;
        return *this;
    }

    csvWriter& operator << (const std::string & val){
        fs << '"' << val << '"' << separator;
        return *this;
    }

    template<typename T>
    csvWriter& operator << (const T& val){
        fs << val << separator;
        return *this;
    }

    ~csvWriter(){
        flush();
        fs.close();
    }
};

inline static csvWriter& endRow(csvWriter& file){
    file.endRow();
    return file;
}

inline static csvWriter& flush(csvWriter& file){
    file.flush();
    return file;
}

#endif //CSVMASTER_CSVWRITER_H
